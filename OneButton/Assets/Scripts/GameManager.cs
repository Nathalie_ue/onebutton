using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject prefab, button, findMeText;

    [SerializeField] private int clickCounter, levelCounter;

    [SerializeField] private float timer;

    [SerializeField] private Vector3 firstButtonSize, secondButtonSize, thirdButtonSize, fourthButtonSize;
    
    [SerializeField] private List<GameObject> cubes = new List<GameObject>();

    [SerializeField] private TextMeshProUGUI levelNumberText, timerText;
    

    private void Start()
    {
        findMeText.SetActive(true);
        clickCounter = 0;

        button.transform.localScale = firstButtonSize;
        prefab.transform.localScale = firstButtonSize;

        gameFinish.SetActive(false);

        levelCounter = 0;
        timer = 60;
    }

    // Click the "Instantiate!" button and a new `prefab` will be instantiated
    // somewhere within -10.0 and 10.0 (inclusive) on the x-z plane
    public void OnClick()
    {
        if (clickCounter == 0)
        {
            findMeText.SetActive(false);
        }

        if (clickCounter >= 0 && clickCounter <= 4)
        {
            var position1 = new Vector3(Random.Range(-6.49f, 6.49f), Random.Range(-3.48f, 1.78f), 0);
            GameObject newCube = Instantiate(prefab, position1, Quaternion.identity);
            newCube.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube);

            var position2 = new Vector3(Random.Range(-6.49f, 6.49f), Random.Range(-3.48f, 1.78f), 0);
            GameObject newCube2 = Instantiate(prefab, position2, Quaternion.identity);
            newCube2.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube2);

            var position3 = new Vector3(Random.Range(-6.49f, 6.49f), Random.Range(-3.48f, 1.78f), 0);
            GameObject newCube3 = Instantiate(prefab, position3, Quaternion.identity);
            newCube3.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube3);

            var buttonPosition = new Vector3(Random.Range(-254.2f, 254.2f), Random.Range(-136.2f, 69.5f), 0);
            button.transform.localPosition = buttonPosition;
        }
        
        if (clickCounter == 5)
        {
            cubes.ForEach(Destroy);
            cubes.Clear();

            button.transform.localScale = secondButtonSize;
            prefab.transform.localScale = secondButtonSize;

            var position1 = new Vector3(Random.Range(-7f, 7f), Random.Range(-3.99f, 2.28f), 0);
            GameObject newCube = Instantiate(prefab, position1, Quaternion.identity);
            newCube.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube);

            var position2 = new Vector3(Random.Range(-7f, 7f), Random.Range(-3.99f, 2.28f), 0);
            GameObject newCube2 = Instantiate(prefab, position2, Quaternion.identity);
            newCube2.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube2);

            var position3 = new Vector3(Random.Range(-7f, 7f), Random.Range(-3.99f, 2.28f), 0);
            GameObject newCube3 = Instantiate(prefab, position3, Quaternion.identity);
            newCube3.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube3);

            var position4 = new Vector3(Random.Range(-7f, 7f), Random.Range(-3.99f, 2.28f), 0);
            GameObject newCube4 = Instantiate(prefab, position3, Quaternion.identity);
            newCube4.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube4);

            var position5 = new Vector3(Random.Range(-7f, 7f), Random.Range(-3.99f, 2.28f), 0);
            GameObject newCube5 = Instantiate(prefab, position3, Quaternion.identity);
            newCube5.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube5);

            var buttonPosition = new Vector3(Random.Range(-273.6f, 273.6f), Random.Range(-156f, 88.9f), 0);
            button.transform.localPosition = buttonPosition;
        }

        if (clickCounter >= 5 && clickCounter <= 11)
        {
            var position1 = new Vector3(Random.Range(-7f, 7f), Random.Range(-3.99f, 2.28f), 0);
            GameObject newCube = Instantiate(prefab, position1, Quaternion.identity);
            newCube.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube);

            var position2 = new Vector3(Random.Range(-7f, 7f), Random.Range(-3.99f, 2.28f), 0);
            GameObject newCube2 = Instantiate(prefab, position2, Quaternion.identity);
            newCube2.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube2);

            var position3 = new Vector3(Random.Range(-7f, 7f), Random.Range(-3.99f, 2.28f), 0);
            GameObject newCube3 = Instantiate(prefab, position3, Quaternion.identity);
            newCube3.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube3);

            var position4 = new Vector3(Random.Range(-7f, 7f), Random.Range(-3.99f, 2.28f), 0);
            GameObject newCube4 = Instantiate(prefab, position3, Quaternion.identity);
            newCube4.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube4);

            var position5 = new Vector3(Random.Range(-7f, 7f), Random.Range(-3.99f, 2.28f), 0);
            GameObject newCube5 = Instantiate(prefab, position3, Quaternion.identity);
            newCube5.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube5);

            var buttonPosition = new Vector3(Random.Range(-273.6f, 273.6f), Random.Range(-156f, 88.9f), 0);
            button.transform.localPosition = buttonPosition;
        }

        if (clickCounter == 12)
        {
            cubes.ForEach(Destroy);
            cubes.Clear();

            button.transform.localScale = thirdButtonSize;
            prefab.transform.localScale = thirdButtonSize;

            var position1 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube = Instantiate(prefab, position1, Quaternion.identity);
            newCube.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube);

            var position2 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube2 = Instantiate(prefab, position2, Quaternion.identity);
            newCube2.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube2);

            var position3 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube3 = Instantiate(prefab, position3, Quaternion.identity);
            newCube3.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube3);

            var position4 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube4 = Instantiate(prefab, position3, Quaternion.identity);
            newCube4.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube4);

            var position5 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube5 = Instantiate(prefab, position3, Quaternion.identity);
            newCube5.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube5);

            var position6 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube6 = Instantiate(prefab, position3, Quaternion.identity);
            newCube6.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube6);

            var position7 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube7 = Instantiate(prefab, position3, Quaternion.identity);
            newCube7.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube7);

            var position8 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube8 = Instantiate(prefab, position3, Quaternion.identity);
            newCube8.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube8);

            var position9 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube9 = Instantiate(prefab, position3, Quaternion.identity);
            newCube9.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube9);

            var position10 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube10 = Instantiate(prefab, position3, Quaternion.identity);
            newCube10.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube10);

            var position11 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube11 = Instantiate(prefab, position3, Quaternion.identity);
            newCube11.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube11);

            var position12 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube12 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube12);

            var position13 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube13 = Instantiate(prefab, position3, Quaternion.identity);
            newCube10.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube13);

            var position14 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube14 = Instantiate(prefab, position3, Quaternion.identity);
            newCube11.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube14);

            var position15 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube15 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube15);

            var buttonPosition = new Vector3(Random.Range(-293.4f, 293.4f), Random.Range(-175.9f, 108.7f), 0);
            button.transform.localPosition = buttonPosition;
        }

        if (clickCounter >= 12 && clickCounter <= 25)
        {
            var position1 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube = Instantiate(prefab, position1, Quaternion.identity);
            newCube.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube);

            var position2 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube2 = Instantiate(prefab, position2, Quaternion.identity);
            newCube2.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube2);

            var position3 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube3 = Instantiate(prefab, position3, Quaternion.identity);
            newCube3.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube3);

            var position4 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube4 = Instantiate(prefab, position3, Quaternion.identity);
            newCube4.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube4);

            var position5 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube5 = Instantiate(prefab, position3, Quaternion.identity);
            newCube5.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube5);

            var position6 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube6 = Instantiate(prefab, position3, Quaternion.identity);
            newCube6.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube6);

            var position7 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube7 = Instantiate(prefab, position3, Quaternion.identity);
            newCube7.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube7);

            var position8 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube8 = Instantiate(prefab, position3, Quaternion.identity);
            newCube8.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube8);

            var position9 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube9 = Instantiate(prefab, position3, Quaternion.identity);
            newCube9.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube9);

            var position10 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube10 = Instantiate(prefab, position3, Quaternion.identity);
            newCube10.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube10);

            var position11 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube11 = Instantiate(prefab, position3, Quaternion.identity);
            newCube11.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube11);

            var position12 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube12 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube12);

            var position13 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube13 = Instantiate(prefab, position3, Quaternion.identity);
            newCube10.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube13);

            var position14 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube14 = Instantiate(prefab, position3, Quaternion.identity);
            newCube11.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube14);

            var position15 = new Vector3(Random.Range(-7.505f, 7.505f), Random.Range(-4.495f, 2.776f), 0);
            GameObject newCube15 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube15);

            var buttonPosition = new Vector3(Random.Range(-293.4f, 293.4f), Random.Range(-175.9f, 108.7f), 0);
            button.transform.localPosition = buttonPosition;
        }

        if (clickCounter == 26)
        {
            cubes.ForEach(Destroy);
            cubes.Clear();

            button.transform.localScale = fourthButtonSize;
            prefab.transform.localScale = fourthButtonSize;

            var position1 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube = Instantiate(prefab, position1, Quaternion.identity);
            newCube.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube);

            var position2 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube2 = Instantiate(prefab, position2, Quaternion.identity);
            newCube2.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube2);

            var position3 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube3 = Instantiate(prefab, position3, Quaternion.identity);
            newCube3.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube3);

            var position4 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube4 = Instantiate(prefab, position3, Quaternion.identity);
            newCube4.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube4);

            var position5 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube5 = Instantiate(prefab, position3, Quaternion.identity);
            newCube5.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube5);

            var position6 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube6 = Instantiate(prefab, position3, Quaternion.identity);
            newCube6.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube6);

            var position7 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube7 = Instantiate(prefab, position3, Quaternion.identity);
            newCube7.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube7);

            var position8 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube8 = Instantiate(prefab, position3, Quaternion.identity);
            newCube8.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube8);

            var position9 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube9 = Instantiate(prefab, position3, Quaternion.identity);
            newCube9.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube9);

            var position10 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube10 = Instantiate(prefab, position3, Quaternion.identity);
            newCube10.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube10);

            var position11 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube11 = Instantiate(prefab, position3, Quaternion.identity);
            newCube11.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube11);

            var position12 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube12 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube12);

            var position13 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube13 = Instantiate(prefab, position3, Quaternion.identity);
            newCube10.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube13);

            var position14 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube14 = Instantiate(prefab, position3, Quaternion.identity);
            newCube11.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube14);

            var position15 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube15 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube15);

            var position16 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube16 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube16);

            var position17 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube17 = Instantiate(prefab, position3, Quaternion.identity);
            newCube10.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube17);

            var position18 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube18 = Instantiate(prefab, position3, Quaternion.identity);
            newCube11.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube18);

            var position19 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube19 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube19);

            var position20 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube20 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube20);

            var position21 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube21 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube21);

            var position22 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube22 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube22);

            var position23 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube23 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube23);

            var position24 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube24 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube24);

            var position25 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube25 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube25);

            var position26 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube26 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube26);

            var position27 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube27 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube27);

            var position28 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube28 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube28);

            var position29 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube29 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube29);

            var position30 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube30 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube30);

            var position31 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube31 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube31);

            var position32 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube32 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube32);

            var position33 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube33 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube33);

            var position34 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube34 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube34);

            var position35 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube35 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube35);

            var buttonPosition = new Vector3(Random.Range(-303.19f, 303.19f), Random.Range(-185.6f, 118.3f), 0);
            button.transform.localPosition = buttonPosition;
        }

        if (clickCounter >= 26 && clickCounter <= 80)
        {
            var position1 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube = Instantiate(prefab, position1, Quaternion.identity);
            newCube.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube);

            var position2 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube2 = Instantiate(prefab, position2, Quaternion.identity);
            newCube2.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube2);

            var position3 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube3 = Instantiate(prefab, position3, Quaternion.identity);
            newCube3.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube3);

            var position4 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube4 = Instantiate(prefab, position3, Quaternion.identity);
            newCube4.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube4);

            var position5 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube5 = Instantiate(prefab, position3, Quaternion.identity);
            newCube5.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube5);

            var position6 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube6 = Instantiate(prefab, position3, Quaternion.identity);
            newCube6.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube6);

            var position7 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube7 = Instantiate(prefab, position3, Quaternion.identity);
            newCube7.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube7);

            var position8 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube8 = Instantiate(prefab, position3, Quaternion.identity);
            newCube8.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube8);

            var position9 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube9 = Instantiate(prefab, position3, Quaternion.identity);
            newCube9.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube9);

            var position10 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube10 = Instantiate(prefab, position3, Quaternion.identity);
            newCube10.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube10);

            var position11 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube11 = Instantiate(prefab, position3, Quaternion.identity);
            newCube11.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube11);

            var position12 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube12 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube12);

            var position13 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube13 = Instantiate(prefab, position3, Quaternion.identity);
            newCube10.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube13);

            var position14 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube14 = Instantiate(prefab, position3, Quaternion.identity);
            newCube11.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube14);

            var position15 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube15 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube15);

            var position16 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube16 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube16);

            var position17 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube17 = Instantiate(prefab, position3, Quaternion.identity);
            newCube10.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube17);

            var position18 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube18 = Instantiate(prefab, position3, Quaternion.identity);
            newCube11.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube18);

            var position19 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube19 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube19);

            var position20 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube20 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube20);

            var position21 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube21 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube21);

            var position22 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube22 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube22);

            var position23 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube23 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube23);

            var position24 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube24 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube24);

            var position25 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube25 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube25);

            var position26 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube26 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube26);

            var position27 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube27 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube27);

            var position28 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube28 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube28);

            var position29 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube29 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube29);

            var position30 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube30 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube30);

            var position31 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube31 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube31);

            var position32 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube32 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube32);

            var position33 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube33 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube33);

            var position34 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube34 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube34);

            var position35 = new Vector3(Random.Range(-7.79f, 7.79f), Random.Range(-4.75f, 3.04f), 0);
            GameObject newCube35 = Instantiate(prefab, position3, Quaternion.identity);
            newCube12.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 1f, 1f, 1f, 1f);
            cubes.Add(newCube35);

            var buttonPosition = new Vector3(Random.Range(-303.19f, 303.19f), Random.Range(-185.6f, 118.3f), 0);
            button.transform.localPosition = buttonPosition;
        }

        clickCounter++;
        levelCounter++;

        levelNumberText.text = levelCounter.ToString();
    }


    [SerializeField] private string sceneToLoad;
    [SerializeField] private GameObject gameFinish;

    private float internalTimer = 0;

    private void Update()
    {
        timer -= Time.deltaTime;
        timerText.text = timer.ToString("00");

        if(timer <= 0)
        {
            gameFinish.SetActive(true);

            internalTimer += Time.deltaTime;

            if (internalTimer >= 4f)
            {
                SceneManager.LoadScene(sceneToLoad, LoadSceneMode.Single);
            }            
        }
    }
}
